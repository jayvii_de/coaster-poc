#!/usr/bin/env python3

from gi.repository import Gtk
import sys


class CoasterWindow(Gtk.ApplicationWindow):
    # create a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Coaster", application=app)
        self.set_default_size(300, 300)
        self.maximize()

        calendar = Gtk.Calendar()
        self.add(calendar)


class CoasterApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = CoasterWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = CoasterApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)

